let Discord = require("discord.js")
function color() {
  //return '#' + Math.random().toString(16).slice(2, 8);
  return "#7289DA"
  }
const authors = [];
var banned = [];
var messagelog = [];

/**
 * @param  {bot} bot - The discord.js Client/bot
 * @param  {object} options - Optional (Custom configuarion options)
 * @return {[type]}         [description]
 */
module.exports = function (bot, options) {
  const maxBuffer = 5;
  const interval = 4000;
  const maxDuplicatesBan = 4;
  bot.on('message', msg => {
    if (msg.channel.type != "text") return;
    if(msg.author.id != bot.user.id){
      var now = Math.floor(Date.now());
      authors.push({
        "time": now,
        "author": msg.author.id
      });
      messagelog.push({
        "message": msg.content,
        "author": msg.author.id
      });
      var msgMatch = 0;
      for (var i = 0; i < messagelog.length; i++) {
        if (messagelog[i].message == msg.content && (messagelog[i].author == msg.author.id) && (msg.author.id !== bot.user.id)) {
          msgMatch++;
        }
      }
      if (msgMatch >= maxDuplicatesBan && !banned.includes(msg.author.id)) {
        ban(msg, msg.author.id, msgMatch);
      }
      matched = 0;
      for (var i = 0; i < authors.length; i++) {
        if (authors[i].time > now - interval) {
          matched++;
          if (matched == maxBuffer) {
            if (!banned.includes(msg.author.id)) {
              ban(msg, msg.author.id);
            }
          }
        }
        else if (authors[i].time < now - interval) {
          authors.splice(i);
        }
        if (messagelog.length >= 200) {
          messagelog.shift();
        }
      }
    }
  });
  /**
   * @param  {Object} msg
   * @param  {string} userid userid
   * @return {boolean} True or False
   */
  function ban(msg, userid, count) {
    for (var i = 0; i < messagelog.length; i++) {
      if (messagelog[i].author == msg.author.id) {
        messagelog.splice(i);
      }
    }
    banned.push(msg.author.id);
    var user = msg.channel.guild.members.find(member => member.user.id === msg.author.id)
    if (user.highestRole.position >= user.guild.roles.find("name", "Staff").position || user.hasPermission("ADMINISTRATOR")) return;
    if (user) {
      msg.guild.channels.forEach(c => { 
        c.overwritePermissions(user, {SEND_MESSAGES: false}).catch(console.error);
        });
      msg.channel.messages.fetch({limit: 20}).then(i => {
        i = i.filter(n => n.author.id == user.id)
        i = i.filter(n => Date.now() - n.createdTimestamp <= 4000)
      msg.channel.bulkDelete(i)
      })
      const embed = new Discord.MessageEmbed()
        .setColor(color())
        .setTitle("Muted!")
        .setDescription(`<@${msg.author.id}> spammed and got muted!`)
      msg.channel.send({embed})
      let embed2 = new Discord.MessageEmbed()
        .setAuthor(`Author: ${msg.author.username}#${msg.author.discriminator}`, msg.author.avatarURL)
        .setTitle("Mute!")
        .setDescription(`<@${user.id}> just got muted for spamming!`)
        .setTimestamp()
      msg.guild.channels.get("343083599194488834").send({embed: embed2})
    }
  }

}
