// https://discordapp.com/oauth2/authorize?client_id=341593322424369152&scope=bot&permissions=2146958591

const token = require("./config.json").token
const prefix = require("./config.json").prefix
const Discord = require("discord.js")
const Collection = require("djs-collection")
const PersistentCollection = require("djs-collection-persistent")
const chalk = require("chalk")
const jquery = require("jquery")
const moment = require("moment")
const _ = require('lodash')
const jimp = require('jimp')
const fs = require("fs-extra")
const antispam = require("./anti_spam.js")

const client = new Discord.Client({disableEveryone: true})
let levelspam = new Collection()
let info = new PersistentCollection({name: "info"})
// let daily = new PersistentCollection({name: "daily"})
let levels = new PersistentCollection({name: "lvl"})
let roles = new PersistentCollection({name: "roles"})
let punishments = new PersistentCollection({name: "punishments"})
let balance = new PersistentCollection({name: "balance"})
let ignorelogs = ["347108471482875905", "343083599194488834", "338641329410277377"]
if (!info.has("rolerequest")) info.set("rolerequest", []);
if (!info.has("npid")) info.set("npid", "0");
info.set("devlangs", ["", "Arduino", "Assembly", "ActionScript", "Bash", "C", "Command Prompt", "C++", "CSS", "C#", "D", "Dart", "Javascript", "HTML", "Python", "PHP", 
"Perl", "Ruby", "VB", "F#", "SQL", "Java", "Swift", "Go", "R", "LUA", "Haskell", "Erlang", "CoffeeScript", "Matlab", "PowerShell", "TypeScript"])
info.set("langs", ["", "Arabian", "Chinese", "Croatian", "Czech", "English", "French", "German", "Italian", "Japanese", "Korean", "Portugese", "Serbian", "Spanish", "Hindi", 
"Danish", "Norwegian", "Dutch", "Slowakian", "Slovenian", "Hungarian", "Albanian", "Greek", "Bulgarian", "Romanian", "Russian"])
info.set("regions", ["", "Africa", "North America", "South America", "Australia", "Asia", "Europe"])
client.login(token)
antispam(client)
function clean(text) {
  if (typeof(text) === 'string')
    return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));
  	else
      return text;
      } 
function color() {
    //return '#' + Math.random().toString(16).slice(2, 8);
    return "#7289DA"
    }
function ArrInclude(string, array) {
    let res = (array.indexOf(string) > -1) ? true : false
    return res
    }
function getemoji(status) {
    let statuses = ["online", "idle", "offline", "dnd", "streaming", "cross", "check", "bot"]
    if (statuses.indexOf(status) == -1) return null; 
        let emoji = client.guilds.get("343331757120290818").emojis.filter(e => e.name == status).first().id
        return `<:${status}:${emoji}>`
    }
function toTitleCase(str) {
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}
function fillBal(who) {
	let user = (who == 1 || !who) ? "178409772436029440" : `${who}` 
	balance.set(user, {balance: 50000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000}) 
	return "Filled!"
	}
process.on('SIGINT', function() {
    console.log("\n" + chalk.blue("Jack ") + chalk.yellow("just stopped. ") + chalk.red("Have a nice day!"))
    process.exit()
    })
client.on('ready', () => {
    client.user.setActivity("Just another Community | !help", {type: "PLAYING"})
	console.log(`Bot started!\nPrefix: ${chalk.green(prefix)}`)
	setInterval(() => {client.sweepMessages()}, 30000)
	})
client.on('guildCreate', async (guild) => {
    if (guild.id != "335719696592535562") {
        guild.owner.send(`Hey! Someone added me to your server(${guild.name}) recently. However, im a private bot. Because of that, i have left your server. I am only made for 1 guild specifically, and even if i allowed you to use me in your server, i would probably not work as i was designed for that server and its roles, channels and settings. \nThank you for your understanding!`).catch()
        guild.leave()
        }
    })
client.on('guildMemberAdd', async (member) => {
    let cnl = member.guild.channels.get("335719696592535562")
    let embed = new Discord.MessageEmbed()
        .setAuthor(member.displayName, member.user.avatarURL)
        .setTitle("Welcome!")
        .setDescription(`Welcome <@${member.id}>! We hope you enjoy your stay! Please read the <#${member.guild.channels.get("336516109714849803").id}> to keep the server as friendly as possible!`)
    cnl.send({embed})
    let embed2 = new Discord.MessageEmbed()
        .setTitle("Member joined!")
        .setDescription(`A member joined: <@${member.id}>`)
        .setTimestamp()
	member.guild.channels.get("343083599194488834").send({embed: embed2})
	if (!balance.has(member.user.id)) balance.set(member.user.id, {balance: 1000});
    let roleids = roles.get(member.id)
    roleids.forEach(e => member.addRole(e))
    roles.delete(member.id)
    })
client.on('guildMemberRemove', async (member) => {
    let embed = new Discord.MessageEmbed()
        .setTitle("Member left!")
        .setDescription(`A member left: <@${member.id}>`)
        .setTimestamp()
    member.guild.channels.get("343083599194488834").send({embed})
    let arr = []
    member.roles.forEach(e => {if (e.id != member.guild.defaultRole.id) arr.push(e.id);})
    roles.set(member.id, arr)
    })
client.on('messageDelete', async (message) => {
    if (message.channel.type != "text") return;
    if (ignorelogs.indexOf(message.channel.id) > -1) return;
    if (message.author.id == client.user.id) return;
    let embed = new Discord.MessageEmbed()
        .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
        .setTitle("Deleted Message!")
        .setDescription(`A message in <#${message.channel.id}> was deleted! Contents: \`\`\`${message.content}\`\`\``)
        .setTimestamp()
    message.guild.channels.get("343083599194488834").send({embed})
    })
client.on("messageUpdate", async (oldMsg, newMsg) => {
    if (oldMsg.channel.type != "text") return;
    if (ignorelogs.indexOf(oldMsg.channel.id) > -1) return;
    if (oldMsg.author.id == client.user.id) return;
    let embed = new Discord.MessageEmbed()
        .setAuthor(`Author: ${oldMsg.author.username}#${oldMsg.author.discriminator}`, oldMsg.author.avatarURL)
        .setTitle("Edited Message!")
        .setDescription(`A message in <#${oldMsg.channel.id}> was edited! \nOld contents: \`\`\`${oldMsg.content}\`\`\`\nNew contents: \`\`\`${newMsg.content}\`\`\``)
        .setTimestamp()
    oldMsg.guild.channels.get("343083599194488834").send({embed})
    })
client.on('message', async (message) => {
    let jac = client.guilds.get("335719696592535562")
    if (message.channel.type == "dm") {
        let arr = info.get("rolerequest")
        if (arr.indexOf(message.author.id) > -1) {
            let devroles = info.get("devlangs")
            let langs = info.get("langs")
            let regions = info.get("regions")
            let roles = regions.concat(devroles.concat(langs))
            if (roles.indexOf(message.content) > -1) {
                if (jac.member(message.author).roles.has(jac.roles.find("name", message.content).id)) {
                    jac.member(message.author).removeRole(jac.roles.find("name", message.content))
                    message.channel.send("Role removed!")
                    } else {
                    jac.member(message.author).addRole(jac.roles.find("name", message.content))
                    message.channel.send("Role added!")
                    }
                    } else if (message.content.toLowerCase() == "end") {
                    arr.splice(arr.indexOf(message.author.id), 1)
                    info.set("rolerequest", arr)
                    message.channel.send("Ended selection!")
                    } else {
                    message.channel.send("Invaild role! Roles are case-sensitive!")
                    }
            }
        }
    if (message.author.bot) return;
    if (message.member && message.channel.type == "text") {
        if (message.channel.id == "338641329410277377") return;
        if (levels.has(message.author.id)) {
            let oldstr = levelspam.get(message.author.id) || message.content
            let newstr = message.content
            levelspam.set(message.author.id, newstr)
            let lvlobj = levels.get(message.author.id)
            let level = lvlobj.level
            let points = lvlobj.points
            let msgl = lvlobj.msgl
            let msgc = lvlobj.msgc
            let lvlup = (level > 0) ? Math.round((level + 1 * 0.1) * (level * 0.41) * 652) : 1
            points = (message.content.length > 10 && oldstr != newstr) ? points + (Math.floor(Math.random() * (25 - 10 + 1)) + 10) : points
            if (points >= lvlup) {
                level = level + 1
                points = 0
                if (level == 2) {
                    message.member.addRole(jac.roles.find("name", "Newcomer (Level 2)"))
                    const embed = new Discord.MessageEmbed()
                        .setTitle("Level Up!")
                        .setDescription(`Congratulations <@${message.author.id}>! You just leveled up! \nAlso, you hit level 2! You seem to like to chat in our server, so i think you deserve the Newcomer role.`)
                        .addField("Level", level)
                    message.channel.send({embed})
                } else if (level == 5) {
                    message.member.addRole(jac.roles.find("name", "Chatter (Level 5)"))
                    const embed = new Discord.MessageEmbed()
                        .setTitle("Level Up!")
                        .setDescription(`Congratulations <@${message.author.id}>! You just leveled up! \nAlso, you hit level 5! Everything big starts small, but here's your Chatter role, as a gift, from robot to human.`)
                        .addField("Level", level)
                    message.channel.send({embed})
                } else if (level == 10) {
                    message.member.addRole(jac.roles.find("name", "Known (Level 10)"))
                    const embed = new Discord.MessageEmbed()
                        .setTitle("Level Up!")
                        .setDescription(`Congratulations <@${message.author.id}>! You just leveled up! \nAlso, you hit level 10! Lets celebrate that. You got the Known role, congratulations!`)
                        .addField("Level", level)
                    message.channel.send({embed})
                } else if (level == 20) {
                    message.member.addRole(jac.roles.find("name", "Speedtyper (Level 20)"))
                    const embed = new Discord.MessageEmbed()
                        .setTitle("Level Up!")
                        .setDescription(`Congratulations <@${message.author.id}>! You just leveled up! \nAlso, you hit level 20! You have proven yourself as a very active member, so here's your Speedtyper role!`)
                        .addField("Level", level)
                    message.channel.send({embed})
                } else if (level == 30) {
                    message.member.addRole(jac.roles.find("name", "Chat Reviver (Level 30)"))
                    const embed = new Discord.MessageEmbed()
                        .setTitle("Level Up!")
                        .setDescription(`Congratulations <@${message.author.id}>! You just leveled up! \nAlso, you hit level 30! Congratulations, here's your Chat Reviver role!`)
                        .addField("Level", level)
                    message.channel.send({embed})
                } else if (level == 50) {
                    message.member.addRole(jac.roles.find("name", "CH4TT3RB0T (Level 50)"))
                    const embed = new Discord.MessageEmbed()
                        .setTitle("Level Up!")
                        .setDescription(`Congratulations <@${message.author.id}>! You just leveled up! \nAlso, you hit LEVEL 50!!!! I dont know how, but here's your CH4TT3RB0T role, you deserve it!`)
                        .addField("Level", level)
                    message.channel.send({embed})
                } else {
                    if (level == 0) return;
                    const embed = new Discord.MessageEmbed()
                        .setTitle("Level Up!")
                        .setDescription(`Congratulations <@${message.author.id}>! You just leveled up!`)
                        .addField("Level", level)
                    message.channel.send({embed})
                    }
                }
            let obj = {
                "level": level,
                "points": points,
                "author": message.author.id,
                "msgc": (msgc + 1),
                "msgl": (msgl + (message.content.length / msgc))
            }
            levels.set(message.author.id, obj)
        } else {
            let obj = {
                "level": 0,
                "points": 0,
                "author": message.author.id,
                "msgc": (1),
                "msgl": (message.content.length)
            }
            levels.set(message.author.id, obj)
            }
        }
    if (message.channel.type == "text" && !message.channel.id == "347108471482875905" && !message.content.startsWith(prefix)) return;
    if (message.guild && !jac) return;
    if (message.channel.type != "text") return;
    let args = message.content.split(/\s+/g).slice(1)
    let command = message.content.replace(prefix, "").split(/\s+/g)[0].toLowerCase()
    let staff = message.member.highestRole.position >= jac.roles.find("name", "Staff").position || message.member.permissions.has("ADMINISTRATOR")
    let support = message.member.highestRole.position >= jac.roles.find("name", "Support Team").position || message.member.permissions.has("ADMINISTRATOR")
    let moderator = message.member.highestRole.position >= jac.roles.find("name", "Moderation Team").position || message.member.permissions.has("ADMINISTRATOR")
    let management = message.member.highestRole.position >= jac.roles.find("name", "Management Team").position || message.member.permissions.has("ADMINISTRATOR")
    let supervision = message.member.highestRole.position >= jac.roles.find("name", "Supervision Team").position || message.member.permissions.has("ADMINISTRATOR")
    let administration = message.member.highestRole.position >= jac.roles.find("name", "Administration Team").position || message.member.permissions.has("ADMINISTRATOR")
    let leadership = message.member.highestRole.position >= jac.roles.find("name", "Leadership Team").position || message.member.permissions.has("ADMINISTRATOR")
    if (message.mentions && !staff) {
        let size = message.mentions.users.size + message.mentions.roles.size;
        if (size > 5) {
            message.guild.channels.forEach(c => { 
                c.overwritePermissions(message.author.id, {SEND_MESSAGES: false}).catch(console.error);
                });
            const embed = new Discord.MessageEmbed()
                .setColor(color())
                .setTitle("Muted!")
                .setDescription(`<@${message.author.id}> spammed and got muted!`)
            message.channel.send({embed})
            let embed2 = new Discord.MessageEmbed()
                .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
                .setTitle("Mute!")
                .setDescription(`<@${message.author.id}> just got muted for mention spamming!`)
                .setTimestamp()
            message.guild.channels.get("343083599194488834").send({embed: embed2})
            }
        }
    if (message.channel.id == "347108471482875905") {
        if (command == "developer" || command == "language" || command == "region") {
            message.channel.send("Check your DMs!").then(msg => {
                    setTimeout(() => {
                        message.delete()
                        msg.delete()
                    }, 2000)
                })
        } else if (command == "contentcreator") {
            if (message.member.roles.has(jac.roles.find("name", "Content Creator").id)) {
                message.member.removeRole(jac.roles.find("name", "Content Creator"))
                    message.channel.send("Removed!").then(msg => {
                        setTimeout(() => {
                            message.delete()
                            msg.delete()
                        }, 2000)
                    })
            } else {
                message.member.addRole(jac.roles.find("name", "Content Creator"))
                    message.channel.send("Added!").then(msg => {
                        setTimeout(() => {
                            message.delete()
                            msg.delete()
                        }, 2000)
                    })
                }
            } else {
                message.delete()
                return;
            }
        }
    if (!message.content.startsWith(prefix)) return;
    if (command == "purge" && supervision) {
        if (!parseInt(args[0]) || !args || parseInt(args[0]) > 500) return;
        if (args[0] < 100 && args[0] > 101) {
            message.channel.bulkDelete(parseInt(args[0]))
        } else if (args[0] > 100 && args[0] < 201) {
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(2)
            message.channel.bulkDelete(parseInt(args[0] - 101))
        } else if (args[0] > 200 && args[0] < 301) {
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(3)
            message.channel.bulkDelete(parseInt(args[0] - 201))
        } else if (args[0] > 300 && args[0] < 401) {
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(4)
            message.channel.bulkDelete(parseInt(args[0] - 301))
        } else if (args[0] > 400 && args[0] < 501) {
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(99)
            message.channel.bulkDelete(5)
            message.channel.bulkDelete(parseInt(args[0] - 401))
        }
        const embed = new Discord.MessageEmbed()
            .setTitle("Purged!")
            .setColor(color())
            .setDescription(`Successfully purged ${parseInt(args[0].toString())} messages in <#${message.channel.id}>!`)
        message.channel.send({embed}).then(e => {
            setTimeout(() => e.delete(), 3000)
        })
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Purge!")
            .setDescription(`<#${message.channel.id}> just got purged, and ${args[0]} messages were removed!`)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
       }
    if (command == "mute" && moderator) {
        if (!args || args.length > 1) return;
        let member = jac.member(message.mentions.users.first()) || message.guild.members.get(args[0]) || null;
        if (!member || member == message.author) return;
        if (member.highestRole.position >= message.member.highestRole.position) return;
        message.guild.channels.forEach(c => { 
            c.overwritePermissions(member, {SEND_MESSAGES: false}).catch(console.error);
            });
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle("Muted!")
            .setDescription(`Successfully muted <@${member.id}>!`)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Mute!")
            .setDescription(`<@${member.id}> just got muted!`)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (command == "unmute" && moderator) {
        if (!args || args.length > 1) return;
        let member = jac.member(message.mentions.users.first()) || message.guild.members.get(args[0]) || null;
        message.guild.channels.forEach(c => {
            c.permissionOverwrites.forEach(o => { 
                if (o.id == member.id) o.delete()
                })
            });
        const embed = new Discord.MessageEmbed()
            .setTitle("Unmuted!")
            .setColor(color())
            .setDescription(`Successfully unmuted <@${member.id}>!`)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Unmute!")
            .setDescription(`<@${member.id}> just got unmuted!`)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (command == "lock" && supervision) {
        message.channel.overwritePermissions(message.guild.defaultRole, {SEND_MESSAGES: false})
        const embed = new Discord.MessageEmbed()
            .setTitle("Locked!")
            .setColor(color())
            .setDescription(`Successfully locked <#${message.channel.id}>!`)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Lock!")
            .setDescription(`<#${message.channel.id}> just got locked!`)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (command == "unlock" && supervision) {
        message.channel.overwritePermissions(message.guild.defaultRole, {SEND_MESSAGES: true})
        const embed = new Discord.MessageEmbed()
            .setTitle("Unlocked!")
            .setColor(color())
            .setDescription(`Successfully unlocked <#${message.channel.id}>!`)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Unlock!")
            .setDescription(`<#${message.channel.id}> just got unlocked!`)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (ArrInclude(command, ["profile", "user", "userinfo"])) {
        let user = message.mentions.users.first() || client.users.get(args[0]) || message.author
        let member = message.guild.member(user)
        let gamestring = (!user.presence.activity) ? "None" : toTitleCase(user.presence.activity.type) + " **" + user.presence.activity.name + "**"
        let statusstring = (user.presence.status == "dnd") ? "Do Not Disturb" : toTitleCase(user.presence.status)
        let level = levels.get(user.id) || {level: 0, points: 0}
        let money = balance.get(user.id) || {balance: 0}
        const embed = new Discord.MessageEmbed()
            .setTitle("Userinfo")
            .setColor(color())
            .setDescription(`Want some info about <@${member.id}>? Alright!`)
            .addField("Name", `${user.username}#${user.discriminator}`)
            .addField("Status", getemoji(user.presence.status) + statusstring)
            .addField("ID", user.id)
            .addField("Activity", await gamestring)
            .addField("Created", user.createdAt.toUTCString())
            .addField("Highest Role", `<@&${member.highestRole.id}>`)
            .addField("Level", `${level.level}`)
            .addField("Balance", `$${money.balance}.00`)
            .addField("Joined", member.joinedAt.toUTCString())
        message.channel.send({embed})
        }
    if (command == "help") {
        const embed1 = new Discord.MessageEmbed()
            .setTitle("Help? Help!")
            .setColor(color())
            .setDescription(`HELP IS COMING!!!!!!\n(required args) / [optional args]`)
        const embed2 = new Discord.MessageEmbed()
            .setColor("#ff0000")
            .setTitle("General")
            .addField("help", "This. \nUsage: !help\nPermission level: `@everyone`")
            .addField("ping", "Ping me! \nUsage: !ping\nPermission level: `@everyone`")
            .addField("color", "Generate/preview HEX color. \nUsage: !color [HEX Color]\nPermission level: `@everyone`")
            .addField("serverinfo", "Server info. \nUsage: !serverinfo\nPermission level: `@everyone`")
            .addField("userinfo", "Gives you info about you, him or her. \nUsage: !userinfo [User Mention or ID]\nPermission level: `@everyone`")
            .addField("yomomma **/** yomama", "Sends a random yo momma joke. \nUsage: !yomomma\nPermission level: `@everyone`")
        const embed3 = new Discord.MessageEmbed()
            .setColor("#ffff00")
            .setTitle("Roles")
            .addField("developer", "Are you a developer? \nUsage: !developer (only works in <#347108471482875905>)\nPermission level: `@everyone`")
            .addField("language", "Do you speak a language? \nUsage: !language (only works in <#347108471482875905>)\nPermission level: `@everyone`")
            .addField("region", "Do you live on a continent? \nUsage: !region (only works in <#347108471482875905>)\nPermission level: `@everyone`")
            .addField("contentcreator", "Are you a content creator? \nUsage: !developer (only works in <#347108471482875905>)\nPermission level: `@everyone`")
        const embed4 = new Discord.MessageEmbed()
            .setColor("#00ff00")
            .setTitle("Stats and Money")
            .addField("see", "See another user's punishments. \nUsage: !see (User Mention)\nPermission level: `@everyone`")
            .addField("punishment", "See more info about a single punishment. \nUsage: !punishment (Punishment ID)\nPermission level: `@everyone`")
            .addField("level **/** rank", "Get your or his/her level. \nUsage: !level [User Mention or ID]\nPermission level: `@everyone`")
            .addField("leaderboard", "Top 10 most chatting members. \nUsage: !leaderboard [User Mention or ID]\nPermission level: `@everyone`")
            .addField("bal **/** balance **/** money **/** bank", "See someone's balance. \nUsage: !balance [User Mention or ID]\nPermission level: `@everyone`")
			.addField("pay **/** transfer", "Pay someone money. \nUsage: !pay (User Mention or ID) (Amount)\nPermission level: `@everyone`")
			.addField("gamble", "Gamble. Win or loose. \nUsage: !gamble (Amount)\nPermission level: `@everyone`")
        const embed5 = new Discord.MessageEmbed()
            .setColor("#00ffff")
            .setTitle("Moderation")
            .addField("purge", "Purges chat. \nUsage: !purge (Amount)\nPermission level: `@Moderation Team`")
            .addField("mute", "Mutes a member. \nUsage: !mute (User Mention)\nPermission level: `@Moderation Team`")
            .addField("unmute", "Unmutes a member. \nUsage: !unmute (User Mention)\nPermission level: `@Moderation Team`")
            .addField("punish", "Punishes a member. \nUsage: !punish (User Mention)\nPermission level: `@Moderation Team`")
            .addField("pardon", "Removes a punishment. \nUsage: !pardon (Punishment ID)\nPermission level: `@Moderation Team`")
            .addField("ban", "Ban a member. \nUsage: !ban (User Mention or ID)\nPermission level: `@Moderation Team`")
            .addField("unban", "Unban a member. \nUsage: !unban (User ID)\nPermission level: `@Moderation Team`")
            .addField("kick", "Kick a member. \nUsage: !kick (User Mention or ID)\nPermission level: `@Moderation Team`")
            .addField("pardonall", "Removes all punishments from a member. \nUsage: !pardonall (User Mention or ID)\nPermission level: `@Moderation Team`")
            .addField("lock", "Locks the channel. \nUsage: !lock\nPermission level: `@Supervision Team`")
            .addField("unlock", "Unocks the channel. \nUsage: !unlock\nPermission level: `@Supervision Team`")
        message.author.send({embed: embed1})
        message.author.send({embed: embed2})
        message.author.send({embed: embed3})
        message.author.send({embed: embed4}).then(msg => {
            if (!staff) {
                message.channel.send("Check your DMs! :thumbsup:")
                }
            })  
        if (staff) message.author.send({embed: embed5}).then(msg => {
            message.channel.send("Check your DMs! :thumbsup:")
            	})  
        	}
    if (command == "ping") {
        let latency = Date.now()
        const embed  = new Discord.MessageEmbed()
            .setColor(color())
            .setTimestamp()
            .addField(':ping_pong: Pinging...', 'measuring latency...', true)
        message.channel.send({embed}).then(msg => {
            const embed  = new Discord.MessageEmbed()
                .setColor(color())
                .setTimestamp()
                .addField(':ping_pong: Pong!', Date.now() - latency + 'ms', true)
        msg.channel.send({embed})
        msg.delete()
        });
        }
    if (command == "developer") {
        let devlangs = info.get("devlangs")
        let arr = info.get("rolerequest")
        if (arr.indexOf(message.member.id) == -1) {
            arr.push(message.author.id)
            info.set("rolerequest", arr)
            } else {
            message.channel.send("You are already in select mode!").then(msg => {
                setTimeout(() => {
                    message.delete()
                    msg.delete()
                }, 2000)
            })
            return;
            }
        const embed = new Discord.MessageEmbed()
            .setTitle("Developer")
            .setColor(color())
            .setDescription(`Are you a developer? Please send me the names of the languages you develop in. If you want to remove a developer role you already have, send me its name. Currently we have roles for the following languages: \`\`\`${devlangs.join("\n")}\`\`\`Send end to confirm and end the selection.`)
        message.author.send({embed})
        }
    if (command == "language") {
        let langs = info.get("langs")
        let arr = info.get("rolerequest")
        if (arr.indexOf(message.member.id) == -1) {
            arr.push(message.author.id)
            info.set("rolerequest", arr)
            } else {
            message.channel.send("You are already in select mode!").then(msg => {
                setTimeout(() => {
                    message.delete()
                    msg.delete()
                }, 2000)
            })
            return;
            }
        const embed = new Discord.MessageEmbed()
            .setTitle("Language")
            .setColor(color())
            .setDescription(`Do you wanna show what languages you know? Please send me the names of these languages. If you want to remove a language role you already have, send me its name. Currently we have roles for the following languages: \`\`\`${langs.join("\n")}\`\`\`Send end to confirm and end the selection.`)
        message.author.send({embed})
        }
    if (command == "region") {
        let regions = info.get("regions")
        let arr = info.get("rolerequest")
        if (arr.indexOf(message.member.id) == -1) {
            arr.push(message.author.id)
            info.set("rolerequest", arr)
            } else {
            message.channel.send("You are already in select mode!").then(msg => {
                setTimeout(() => {
                    message.delete()
                    msg.delete()
                }, 2000)
            })
            return;
            }
        const embed = new Discord.MessageEmbed()
            .setTitle("Region")
            .setColor(color())
            .setDescription(`Where do you live? Please send me the names of the region(s) you live in. If you want to remove a region role you already have, send me its name. Currently we have roles for the following regions: \`\`\`${regions.join("\n")}\`\`\`Send end to confirm and end the selection.`)
        message.author.send({embed})
        }

    if (command == "!") {
        if (message.author.id != "178409772436029440") return;
		let code = args.join(" ")
  		try {
      		let evaled = await eval(code);
			if (typeof evaled !== "string") evaled = require("util").inspect(evaled);
			evaled = evaled.replace(new RegExp(token, "g"), "The bot token was filtered for security reasons. ");
			message.channel.send(`\`\`\`xl\n${clean(evaled)}\n\`\`\``);
  		    }
  		catch(err) {
      		message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
  		    }
        }
    if (ArrInclude(command, ["level", "rank"])) {
        let user = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]) || message.member
        if (!levels.has(user.id)) {
            const embed = new Discord.MessageEmbed()
                .setTitle("404 No level...")
                .setColor(color())
                .setDescription(`Sadly, <@${user.id}> has no level :(`)
            message.channel.send({embed})
            return;
        }
        let lvlobj = levels.get(user.id)
        let points = lvlobj.points
        let level = lvlobj.level
        let nxtlvl = (level > 0) ? Math.round((level + 1 * 0.1) * (level * 0.41) * 652) - points : 1
        const embed = new Discord.MessageEmbed()
            .setTitle("Get by mah' level!")
            .setColor(color())
            .setDescription(`Here's the level for <@${user.id}>!`)
            .addField("Level", level)
            .addField("Message Count", lvlobj.msgc)
            .addField("Average Message Length", Math.round(lvlobj.msgl))
            .addField("Points to next level", nxtlvl)
        message.channel.send({embed})
        }
    if (command == "leaderboard") {
        let objs = []
        levels.array().forEach(obj => {
            let correct = JSON.parse("{" + obj.slice(1, -1) + "}")
            objs.push(correct)
            })
        if (objs.length > 10) objs.length = 10;
        let b = ""
        objs = _.orderBy(objs, ["level"], ["desc"])
        objs.forEach(e => {
            b = `${b}<@${e.author}> **>** Level: **${e.level}**\n`
            })
        const embed = new Discord.MessageEmbed()
            .setTitle("Leaderboard!")
            .setColor(color())
            .setDescription(b)
        message.channel.send({embed})
        }
    if (command == "punish" && moderator) {
        let npid = info.get("npid")
        let member
        if (message.guild.member(message.mentions.users.first())) {
            member = {
                "id": message.guild.member(message.mentions.users.first()).id
            }
        } else if (message.guild.members.get(args[0])) {
            member = {
                "id": message.guild.members.get(args[0]).id
            }
        } else {
            member = {
                "id": args[0] || null
            }  
        }
        if (!member.id) return;
        if (message.guild.members.get(member.id) && message.guild.members.get(member.id).highestRole.position >= message.member.highestRole.position) return;
        let reason = args.slice(1).join(" ")
        if (!reason) return;
        let obj = {
            "id": npid,
            "mid": member.id,
            "type": 1,
            "reason": reason,
            "time": message.createdAt.toUTCString(),
            "issuer": message.author.id
            }
        let arr = (punishments.has(member.id)) ? punishments.get(member.id) : []
        arr.push(obj)
        punishments.set(member.id, arr)
        info.set("npid", (npid + 1))
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle("Punished!")
            .setDescription(`Successfully punished <@${member.id}>!`)
            .setFooter(`ID: ${npid}. User now has ${arr.length} punishments. `)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Punish!")
            .setDescription(`<@${member.id}> just got punished:\nReason: \`${reason}\`\nIssuer: <@${message.author.id}>\nTime: \`${obj.time}\`\nID: \`${npid}\``)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (command == "pardon" && moderator) {
        if (!parseInt(args[0])) return;
        let arr = _.flattenDeep(JSON.parse("[" + punishments.array().join(",") + "]"))
        let i = arr.findIndex(i_l => i_l.id == args[0])
        if (i == -1) return;
        let mid = arr[i].mid
        arr.splice(i, 1)
        arr = arr.filter(i_l => i_l.mid == mid)
        if (message.guild.members.get(mid) && message.guild.members.get(mid).highestRole.position >= message.member.highestRole.position) return;
        (arr.length > 0) ? punishments.set(mid, arr) : punishments.delete(mid)
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle("Pardone moi!")
            .setDescription(`Successfully removed punishment ${args[0]} from <@${mid}>!`)
            .setFooter(`User now has ${arr.length} punishments.`)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Pardon!")
            .setDescription(`Punishment ${args[0]} got removed.`)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (command == "pardonall" && moderator) {
        let member
        if (message.guild.member(message.mentions.users.first())) {
            member = {
                "id": message.guild.member(message.mentions.users.first()).id
            }
        } else if (message.guild.members.get(args[0])) {
            member = {
                "id": message.guild.members.get(args[0]).id
            } 
        } else {
            member = {
                "id": args[0] || null
            }  
        }
        if (!member.id) return;
        if (message.guild.members.get(member.id) && message.guild.members.get(member.id).highestRole.position >= message.member.highestRole.position) return;
        punishments.delete(member.id)
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle("Pardone moi!")
            .setDescription(`Successfully removed all punishments from <@${member.id}>!`)
            .setFooter(`User now has 0 punishments.`)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Pardon All!")
            .setDescription(`Removed all punishments from <@${member.id}>`)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (ArrInclude(command, ["see", "punishments", "search"])) {
        if (message.guild.member(message.mentions.users.first())) {
                member = {
                    "id": message.guild.member(message.mentions.users.first()).id
                }
            } else if (message.guild.members.get(args[0])) {
                member = {
                    "id": message.guild.members.get(args[0]).id
                }
            } else {
                member = {
                    "id": args[0] || message.author.id
                }  
            }
        if (!punishments.has(member.id)) {
            const embed = new Discord.MessageEmbed()
                .setTitle("Punishments!")
                .setColor(color())
                .setDescription(`<@${member.id}> has no punishments, yay!`)
            message.channel.send({embed})
            return;
        }
        let arr = punishments.get(member.id)
        let b = ""
        arr.forEach(e => {
            let type = (e.type == 1) ? "Warning" : (e.type == 2) ? "Ban" : (e.type == 3) ? "Kick" : (e.type == 4) ? "Hackban" : "Unknown"
            b = `${b}\n${e.id} (${type}): ${e.reason}`
        })
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle("Punishments!")
            .setDescription(`Punishments for <@${member.id}>: \`\`\`${b}\`\`\``)
            .setFooter(`User has ${arr.length} punishments.`)
        message.channel.send({embed})
        }
    if (command == "punishment") {
        if (!parseInt(args[0])) return;
        let arr = _.flattenDeep(JSON.parse("[" + punishments.array().join(",") + "]"))
        let i = arr.findIndex(i_l => i_l.id == args[0])
        if (i == -1) return;
        let obj = arr[i]
        let type = (e.type == 1) ? "Warning" : (e.type == 2) ? "Ban" : (e.type == 3) ? "Kick" : (e.type == 4) ? "Hackban" : "Unknown"
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle(`Punishment ${obj.id}!`)
            .setDescription(`Info about Punishment Nr. ${obj.id}`)
            .addField(`Punished Member`, `<@${obj.mid}>`)
            .addField(`Type`, type)
            .addField(`Issuer`, `<@${obj.issuer}>`)
            .addField(`Time`, obj.time)
            .addField(`Reason`, obj.reason)
        message.channel.send({embed})
        }
    if (ArrInclude(command, ["yomomma", "yomama"])) {
        let jokes = fs.readFileSync("jokes/yomomma.txt").toString().split("\n")
        let res = jokes[Math.floor(Math.random() * (jokes.length - 1))]
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle(res)
        message.channel.send({embed})
        }
    if (command == "color") {
        let rcolor = (args[0]) ? args[0].replace("#", "").replace("0x","") : Math.random().toString(16).slice(2, 8)
        rcolor.length = 6
        let ocolor = rcolor.toUpperCase()
        rcolor = "0x" + rcolor + "ff"
        var image = new jimp(448, 72, parseInt(rcolor), function (err, image) {
            image.getBuffer(jimp.MIME_PNG, (err, buffer) => {
                const embed = new Discord.MessageEmbed()
                .attachFiles([{attachment: buffer, name: "image.png"}])
                .setImage("attachment://image.png")
                .setTitle("#" + ocolor)
                .setColor("#" + ocolor)
                message.channel.send({embed})
                })
            });
        }
    if (command == "ban" && moderator) {
        let member = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]) || null;
        let reason = args.slice(1).join(" ")
        if (!reason || !member) return;
        if (member.highestRole.position >= message.member.highestRole.position) return;
        let npid = info.get("npid")
        const embed3 = new Discord.MessageEmbed()
            .setTitle("You've been banned :(")
            .setDescription(`Hey ${member.user.username}, sadly, you have been banned from Just another Community for: ${reason}. If you think this ban was unfair, please appeal on our website: coming://soon.tm/`)
        member.user.send({embed: embed3})
        member.ban(7)
        let obj = {
            "id": npid,
            "mid": member.id,
            "type": 2,
            "reason": `${reason}`,
            "time": message.createdAt.toUTCString(),
            "issuer": message.author.id
            }
        let arr = punishments.get(member.id) || []
        arr.push(obj)
        punishments.set(member.id, arr)
        info.set("npid", (npid + 1))
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle("Banned!")
            .setDescription(`<@${member.id}> was banned for: ${reason}.`)
            .setFooter(`ID: ${npid}. User now has ${arr.length} punishments. `)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Ban!")
            .setDescription(`<@${member.id}> just got banned:\nReason: \`${reason}\`\nIssuer: <@${message.author.id}>\nTime: \`${obj.time}\`\nID: \`${npid}\``)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (command == "unban" && moderator) {
        let member = args[0]
        if (!member) return;
        message.guild.unban(member)
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle("Unbanned!")
            .setDescription(`<@${member}> was unbanned!`)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Unban!")
            .setDescription(`<@${member}> just got unbanned!`)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (command == "kick" && moderator) {
        let member = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]) || null;
        let reason = args.slice(1).join(" ")
        if (!reason || !member) return;
        if (member.highestRole.position >= message.member.highestRole.position) return;
        let npid = info.get("npid")
        message.guild.channels.get("335719696592535562").createInvite({maxUses: 1, maxAge: 86400}).then(e => {
            const embed3 = new Discord.MessageEmbed()
                .setTitle("You've been kicked :(")
                .setDescription(`Hey ${member.user.username}, sadly, you have been kicked from Just another Community for: ${reason}. If you wish to rejoin, you can use this single-use invite link: https://discord.gg/${e.code}`)
            member.user.send({embed: embed3}).then(() => {
                member.kick(7)
                })
            })
        let obj = {
            "id": npid,
            "mid": member.id,
            "type" : 3,
            "reason": `${reason}`,
            "time": message.createdAt.toUTCString(),
            "issuer": message.author.id
            }
        let arr = punishments.get(member.id) || []
        arr.push(obj)
        punishments.set(member.id, arr)
        info.set("npid", (npid + 1))
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle("Kicked!")
            .setDescription(`<@${member.id}> was kicked for: ${reason}.`)
            .setFooter(`ID: ${npid}. User now has ${arr.length} punishments. `)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Kick!")
            .setDescription(`<@${member.id}> just got kicked:\nReason: \`${reason}\`\nIssuer: <@${message.author.id}>\nTime: \`${obj.time}\`\nID: \`${npid}\``)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (command == "hackban" && moderator) {
        let toban = args[0] || null;
        let reason = args.slice(1).join(" ")
        if (!toban || !reason) return;
        let npid = info.get("npid")
        message.guild.ban(toban)
        let obj = {
            "id": npid,
            "mid": toban,
            "type": 4,
            "reason": `${reason}`,
            "time": message.createdAt.toUTCString(),
            "issuer": message.author.id
            }
        let arr = punishments.get(toban) || []
        arr.push(obj)
        punishments.set(toban, arr)
        info.set("npid", (npid + 1))
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .setTitle("Hackbanned!")
            .setDescription(`<@${toban}> was hackbanned for: ${reason}.`)
            .setFooter(`ID: ${npid}. User now has ${arr.length} punishments. `)
        message.channel.send({embed})
        let embed2 = new Discord.MessageEmbed()
            .setAuthor(`Author: ${message.author.username}#${message.author.discriminator}`, message.author.avatarURL)
            .setTitle("Ban!")
            .setDescription(`<@${toban}> just got hackbanned:\nReason: \`${reason}\`\nIssuer: <@${message.author.id}>\nTime: \`${obj.time}\`\nID: \`${npid}\``)
            .setTimestamp()
        message.guild.channels.get("343083599194488834").send({embed: embed2})
        }
    if (command == "serverinfo") {
        let rolecount = message.guild.roles
        const embed = new Discord.MessageEmbed()
            .setColor(color())
            .addField("Name", message.guild.name)
            .addField("ID", message.guild.id)
            .addField("Created", message.guild.createdAt.toUTCString())
            .addField("Owner", message.guild.owner.toString())
            .addField("Owner ID", message.guild.ownerID)
            .addField("Region", message.guild.region)
            .addField("Members", `Total: ${message.guild.memberCount} \
            \n   ${getemoji("online")}: ${message.guild.members.filter(e => e.presence.status == "online" && !e.user.bot).size}  \
            \n   ${getemoji("idle")}: ${message.guild.members.filter(e => e.presence.status == "idle" && !e.user.bot).size} \
            \n   ${getemoji("dnd")}: ${message.guild.members.filter(e => e.presence.status == "dnd" && !e.user.bot).size} \
            \n   ${getemoji("offline")}: ${message.guild.members.filter(e => e.presence.status == "offline" && !e.user.bot).size} \
            \n   ${getemoji("bot")}: ${message.guild.members.filter(e => e.user.bot).size} \
            `)
            .addField("Channels", message.guild.channels.size)
            .addField("Roles", )
            
        message.channel.send({embed})
        }
    if (ArrInclude(command, ["bal", "money", "balance", "bank"])) {
        let member = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]) || message.member
        const money = balance.get(member.id) || { balance: 0 }
        let str = (member == message.member) ? `You have **$${money.balance}.00** on your account.` : `<@${member.id}> currently has **$${money.balance}.00** on their account.`
        const embed = new Discord.MessageEmbed()
            .setTitle("Balance")
            .setColor(color())
            .setDescription(str)
        message.channel.send({embed})
        }
    if (ArrInclude(command, ["pay", "transfer"])) {
        let reciever = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]) || null
        let sender = message.member || null
        let amount = Math.round(parseInt(args[1])) || null
        if (!reciever || !sender || !amount || reciever == sender || amount < 0) return;
        const senderbal = balance.get(sender.id) || { balance: 0 }
        const recieverbal = balance.get(reciever.id) || { balance: 0 }
        if (amount > senderbal.balance) {
            const embed = new Discord.MessageEmbed()
                .setTitle("Transfer")
                .setColor(color())
                .setDescription(`Hey! You cant transfer more than you have! You currently only have **$${senderbal.balance}.00** out of the required **$${amount}.00**!`)
            message.channel.send({embed})
            return
            }
        if (amount <= senderbal.balance) {
            balance.set(reciever.id, {balance: (recieverbal.balance + amount)})
            balance.set(sender.id, {balance: (senderbal.balance - amount)})
            const embed = new Discord.MessageEmbed()
                .setTitle("Transfer")
                .setColor(color())
                .setDescription(`Successfully transfered **$${amount}.00** to <@${reciever.id}>. You now have **$${balance.get(sender.id).balance}.00** left on your account.`)
            message.channel.send({embed})
            return
            }
        }
    if (command == "gamble") {
        let res = _.sample([false, false, true, false, true, false, false])
        let amount = parseInt(args[0])
		let money = balance.get(message.author.id) || {balance: 0}
		if (!amount || amount < 0) return;
        if (amount > money.balance) {
            let embed = new Discord.MessageEmbed()
                .setColor(color())
                .setTitle("Gamble")
				.setDescription(`Hey! You cant gamble more than you already own. You currently own **$${money.balance}.00**.`)
			message.channel.send({embed})
			return
        	}
        if (res) {
			balance.set(message.author.id, {balance: (money.balance + amount)})
            let embed = new Discord.MessageEmbed()
                .setColor(color())
                .setTitle("Gamble")
				.setDescription(`**You won**! Added **$${amount}.00** to your account!`)
			message.channel.send({embed})
        } else if (!res) {
			balance.set(message.author.id, {balance: (money.balance - amount)})
            let embed = new Discord.MessageEmbed()
                .setColor(color())
                .setTitle("Gamble")
				.setDescription(`**Sadly, you lost**! Removed **$${amount}.00** from your account!`)
			message.channel.send({embed})
			}
		}
	/*if (command == "daily") {
		let last = daily.get(message.author.id) || Date.now()
		if (daily.has(message.author.id)) {
			if (Date.now() - last < 86400000) {
				let embed = new Discord.MessageEmbed()
                .setColor(color())
                .setTitle("Daily")
				.setDescription(`Hey! 24h have not passed yet. Try again in ${moment(last - (Date.now() + 86400000).format("HH[ hours, ]mm[ minutes and ]ss[ seconds]"))}.`)
			message.channel.send({embed})
				}
			} else {
				daily.set(message.author.id, Date.now())
				let embed = new Discord.MessageEmbed()
                	.setColor(color())
                	.setTitle("Daily")
					.setDescription(`Okay, you will recieve daily money from now on.`)
			message.channel.send({embed})
			}
		}*/
    })